
Admin UX 8.x-1.x, xxxx-xx-xx
----------------------------


Admin UX 7.x-1.x, xxxx-xx-xx
----------------------------
by sun: Added expanding of most recent test group by default.
by sun: Added internal test class names for DX.
by sun: Added duplication of SimpleTest "Run tests" button.
by sun: Added automatic clearing of test environment/results.
by sun: Added SimpleTest recently used groups separation.
by sun: Added initial baseline of module files.
