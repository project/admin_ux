
-- SUMMARY --

Improves the administrative user experience.

For a full description of the module, visit the project page:
  http://drupal.org/project/admin_ux
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/admin_ux


-- REQUIREMENTS --

* @todo


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* @todo


-- USAGE --

* @todo


-- CONTACT --

Current maintainers:
* Daniel F. Kudwien (sun) - http://drupal.org/user/54136

